<?php


	if (isset($_POST["submit"])) {

		// get data
		$username = $_POST["username"];
		$email = $_POST["email"];
		$password1 = $_POST["password1"];
		$password2 = $_POST["password2"];


		//instantiate signconstr class
		include "../class/dbh.classes.php";
		include "../class/signup.classes.php";
		include "../class/signup-contr.classes.php";
		$signup = new SignupContr($username, $email, $password1, $password2);


		//running error handlers and user signup
		$signup->signupUser();

		//Going back to front page
		
		header("location: ../index.php?error=none");
		
	}