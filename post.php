<?php
	session_start();	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Task</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
	<header class="header-container">
		<nav>
			<div class="header-wrapper">
				<div class="logo-Wrapper">
					<h1>MiniBlog</h1>
				</div>
				<div class="navigation-Wrapper">
					<?php

					if (isset($_SESSION['userId'])) 
					{
					?>
					<a href="#" class="login-btn">Hi<?php echo $_SESSION['useruid']; ?></a>
					<a href="./includes/logout.inc.php" class="login-btn" id="logout">logout</a>
					<?php
					}
					else{

					?>
					<a href="#" class="login-btn">Home</a>
					<a href="./includes/logout.inc.php" class="login-btn" id="logout">logout</a>
					<?php
					}
					?>
				</div>
			</div>
		</nav>
	</header>

	<section class="section-container">
		<div class="wrapper">
			<div class="post-Form">
				<div class="post-Title">
					<h1>Login</h1>
				</div>

				<div class="post-Content">
					<p>Contents of the post</p>
				</div>

				<div class="post-Date">
					<p>Date:</p>
				</div>

				<hr>

				<div class="button-wrapper">
					<button type="submit" class="btn-danger">DELETE</button><a href="editPost.php" class="btn-success">EDIT</a>
				</div>	
			</div>
		</div>
	</section>

	<section class="section-container">
		<div class="wrapper">
			<div class="post-Form">
				<div class="button-wrapper btn-CreatePost">
					<a href="createPost.php" class="btn-information">CREATE NEW POST</a>
				</div>
			</div>
		</div>
	</section>
</body>
</html>