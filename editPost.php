<?php
	session_start();	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Task</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
<header class="header-container">
		<nav>
			<div class="header-wrapper">
				<div class="logo-Wrapper">
					<h1>MiniBlog</h1>
				</div>
				<div class="navigation-Wrapper">
					<?php

					if (isset($_SESSION['userId'])) 
					{
					?>
					<a href="#" class="login-btn">Hi<?php echo $_SESSION['useruid']; ?></a>
					<a href="./includes/logout.inc.php" class="login-btn" id="logout">logout</a>
					<?php
					}
					else{

					?>
					<a href="#" class="login-btn">Home</a>
					<a href="./includes/logout.inc.php" class="login-btn" id="logout">logout</a>
					<?php
					}
					?>
				</div>
			</div>
		</nav>
	</header>

	<section class="section-container">
		<div class="wrapper">
			<div class="createPost-Form">
				<h4>Edit Post - Post Title</h4>
				<form>
					<div class="input-wrapper">
						<input type="txt" name="postTitle" placeholder="Enter New Title">
					</div>
					<div class="input-wrapper">
						<input type="text" name="postContent" placeholder="Enter New Content">
					</div>

					<div class="button-wrapper">
						<button type="submit">POST</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</body>
</html>