<?php
	session_start();	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Task</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>
	<header class="header-container">
		<nav>
			<div class="header-wrapper">
				<h1>MiniBlog</h1>
				<a href="#" class="login-btn">Login</a>
			</div>
		</nav>
	</header>

	<section class="section-container">
		<div class="wrapper">
			<div class="login-Form">
				<h4>Login</h4>
				<hr>
				<form action="./includes/login.php" method="post">
					<div class="input-wrapper">
						<input type="txt" name="username" placeholder="Enter Email" >
					</div>
					<div class="input-wrapper">
						<input type="password" name="password" placeholder="Enter Password" >
					</div>

					<div class="button-wrapper">
						<button type="submit" name="submit">Login</button><a href="register.php">Register</a>
					</div>
					<p>Currently log out.</p>
				</form>
			</div>
		</div>
	</section>
</body>
</html>