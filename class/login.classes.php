<?php


class Login extends Dbh {

	protected function getUser($username, $password) {
		echo $username;
		echo $password;
		$stmt = $this->connect()->prepare('SELECT password FROM user WHERE username = ? OR email = ?;');


		if (!$stmt->execute(array($username, $password))) {

			$stmt = null;
			header("location: ../index.php?error=stmtfailed");
			exit();
		}

		if ($stmt ->rowCount() == 0) {
			$stmt = null;
			header("location: ../index.php?error=usernotFound1");
			exit();
		}

		$passwordHashed = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$checkPassword = password_verify($password, $passwordHashed[0]["password"]);


		if ($checkPassword == false) {

			$stmt = null;
			header("location: ../index.php?error=wrongPassword!");
			exit();

		}elseif ($checkPassword == true) {
			
			$stmt = $this->connect()->prepare('SELECT * FROM user WHERE username = ? OR email = ? AND password = ?;');

			if (!$stmt->execute(array($username, $username, $password))) {

			$stmt = null;
			header("location: ../index.php?error=stmtfailed");
			exit();

			}

			if ($stmt ->rowCount() == 0) {
				$stmt = null;
				header("location: ../index.php?error=usernotFound2");
				exit();
			}

			$user = $stmt->fetchAll(PDO::FETCH_ASSOC);

			session_start();
			$SESSION["userId"] = $user[0]["idUser"];
			$SESSION["useruid"] = $user[0]["username"];

			$stmt = null;
		}



	}

}
