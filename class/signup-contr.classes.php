<?php


class SignupContr extends Signup{

	private $username;
	private $email;
	private $password1;
	private $password2;



	public function __construct($username, $email, $password1, $password2) {
		$this->username = $username;
		$this->email = $email;
		$this->password1 = $password1;
		$this->password2 = $password2;
	}


	public function signupUser() {

		//echo empty inmput
		if ($this->emptyInputs() === false) {
			
			header("location: ../register.php?error=emptyinput");
			exit();
		}
		//echo invalid username
		if ($this->invalidUsername() === false) {
			header("location: ../register.php?error=username");
			exit();
		}
		//echo invalid email
		if ($this->invalidEmail() === false) {
			header("location: ../register.php?error=email");
			exit();
		}
		//echo invalid password
		if ($this->passwordMatch() === false) {
			header("location: ../register.php?error=password");
			exit();
		}
		//echo username or email taken
		if ($this->userIdTakenCheck() === false) {
			header("location: ../register.php?error=userOrEmailTaken");
			exit();
		}

		$this->setUser($this->username, $this->email, $this->password1);
	}


	private function emptyInputs() {
		$result;
		if (empty($this->username) || empty($this->email) || empty($this->password1) || empty($this->password2)) {
			$result = false;
		}else{
			$result = true;
		}
		return $result;
	}

	private function invalidUsername() {
		$result;
		if (!preg_match("/^[a-zA-Z0-9]*$/", $this->username)) {

			$result = false;

		}else{

			$result = true;

		}

		return $result;
	}

	private function invalidEmail() {
		$result;
		if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {

			$result = false;

		}else{

			$result = true;

		}

		return $result;
	}

	private function passwordMatch() {
		$result;
		if ($this->password1 !== $this->password2) {

			$result = false;

		}else{

			$result = true;

		}

		return $result;
	}


	private function userIdTakenCheck() {
		$result;
		if (!$this->checkUser($this->username, $this->email)) {

			$result = false;

		}else{

			$result = true;

		}

		return $result;
	}

}