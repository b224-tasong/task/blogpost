<?php


class Signup extends Dbh {

	protected function setUser($username, $email, $password1) {
		$stmt = $this->connect()->prepare('INSERT INTO user( username, email, password) VALUES (?,?,?);');

		$hashpassword = password_hash($password1, PASSWORD_DEFAULT); 

		if (!$stmt->execute(array($username, $email, $hashpassword))) {
					$stmt = null;
					header("location: ../index.php?error=stmtfailed");
					exit();
				}else{
					echo '<script>alert("Your account has been created successfully!")</script>';
					header("location: ../index.php?success=created");
					exit();
				}
		$stmt = null;
	}

	
	protected function checkUser($username, $email) {
		$stmt = $this->connect()->prepare('SELECT idUser FROM `user` WHERE username = ? OR email = ?;');

		if (!$stmt->execute(array($username, $email))) {
			$stmt = null;
			header("location: ../index.php?error=stmtfailed");
			exit();
		}


		$resultCheck;
		if ($stmt->rowCount() > 0) {

			$resultCheck = false;

		}else{

			$resultCheck = true;

		}

		return $resultCheck;
	}
	

}