

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>task</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
</head>
<body>

	<header class="header-container">
		<nav>
			<div class="header-wrapper">
				<h1>MiniBlog</h1>
				<a href="#" class="login-btn">Login</a>
			</div>
		</nav>
	</header>

	<section class="section-container">
		<h1>Registration</h1>
		<div class="wrapper">
			<div class="register-Form">
				<h4>See the Registration Rules</h4>
				<hr>
				<form action="./includes/signup.php" method="post">
					<div class="input-wrapper">
						<input type="txt" name="username" placeholder="Enter Username" required>
					</div>
					<div class="input-wrapper">
						<input type="txt" name="email" placeholder="Enter Email" required>
					</div>
					<div class="input-wrapper">
						<input type="password" name="password1" placeholder="Enter Password" required>
					</div>
					<div class="input-wrapper">
						<input type="password" name="password2" placeholder="Confirm Password" required>
					</div>

					<div class="button-wrapper">
						<button type="submit" name="submit">Register</button>
					</div>
					<p>Return to the <a href="index.php">LOGIN PAGE</a></p>
				</form>
			</div>
		</div>
	</section>

</body>
</html>